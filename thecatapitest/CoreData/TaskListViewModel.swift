//
//  SaveListCoreDataViewModel.swift
//  thecatapitest
//
//  Created by Pedro Ferreira on 18/09/21.
//

import Foundation

class TalkListViewModel: ObservableObject {
    @Published var count: String = ""
    @Published var saveData: String = ""
    @Published var tasks: [TaskViewModel] = []
    @Published var voteplus: Int = 0
    
    func getAllTasks(){
        tasks =  CoreDataManager.shared.getAllTasks().map(TaskViewModel.init)
    }
    
    func save() {
        let task = Task(context: CoreDataManager.shared.persistentContainer.viewContext)
        task.saveData = saveData 
        CoreDataManager.shared.save()
        
    }
    
    func sum (){
        voteplus = voteplus + 1
        print(voteplus)
    }
}

struct TaskViewModel {
    let task: Task
    //  let task1: Task
    var idTask: NSObject {
        return task.objectID
    }
    var saveData: String {
        return task.saveData ?? ""
    }
}

