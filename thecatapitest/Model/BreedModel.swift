//
//  post.swift
//  thecatapitest
//
//  Created by Pedro Ferreira on 18/09/21.
//

import Foundation

struct Breed: Codable {
    let id: String
    let name: String
    let description: String

}
