//
//  PostViewModel.swift
//  thecatapitest
//
//  Created by Pedro Ferreira on 18/09/21.
//

import Foundation

class PostListViewModel: ObservableObject {
    @Published var posts = [PostViewModel]()

    init() {
        
        Webservice().getPosts { posts in
            
            if let posts = posts {
                self.posts = posts.map(PostViewModel.init)
            }
        }
    }
}

struct PostViewModel {
    
    var post: Breed
    init(post: Breed) {
        self.post = post
    }
    var id: String {
        return self.post.id
    }
    var name: String {
        return self.post.name
    }
    var temperament: String {
        return self.post.description
    }
    
}
