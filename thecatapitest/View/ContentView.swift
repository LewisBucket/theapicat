//
//  ContentView.swift
//  thecatapitest
//
//  Created by Pedro Ferreira on 18/09/21.
// MARK: WigiLabsTest

import SwiftUI

struct ContentView: View {
    
    init() {
        UINavigationBar.appearance().barTintColor = .clear
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
    }
    
    @ObservedObject private var postListVM = PostListViewModel()
    @ObservedObject private var taskListVM = TalkListViewModel()
    @State private var showModalView = false
    var body: some View {
        
        Text("thecatapi Test").font(.largeTitle)
        NavigationView {
            
            ScrollView(.vertical, showsIndicators: false) {
                
                ForEach(postListVM.posts, id: \.id) { post in
                    
                    VStack() {
                        Text(post.name).font(.title)
                        
                        Text(post.temperament)
                        Spacer(minLength: 25)
                        
                        HStack {
                            Button {
                                print("Like")
                                taskListVM.saveData = post.name + "   Like👍"
                                taskListVM.save()
                                
                            } label: {
                                Text("").foregroundColor(.black)
                                Image(systemName: "hand.thumbsup").font(.title).accentColor(.yellow)
                            }
                            
                            Button {
                                print("Dislike")
                                taskListVM.saveData = post.name + "   Dislike👎"
                                taskListVM.save()
                            } label: {
                                Text("").foregroundColor(.black)
                                Image(systemName: "hand.thumbsdown").font(.title).accentColor(.yellow)
                            }
                        }
                        Spacer(minLength: 20)
                    }
                }
                
            }.background(Color(.white))
            .ignoresSafeArea(.all, edges: .all)
            .navigationBarItems(trailing: Button {
                showModalView = true
            } label: {
                Text("Voted").foregroundColor(.black)
                Image(systemName: "bookmark.fill").font(.title2).accentColor(.yellow)
                
            })
            .sheet(isPresented: $showModalView) {
                
                CoreDataView()
            }
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
