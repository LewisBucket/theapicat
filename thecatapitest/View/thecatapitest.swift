//
//  thecatapitestApp.swift
//  thecatapitest
//
//  Created by Pedro Ferreira on 18/09/21.
// MARK: WigiLabsTest

import SwiftUI

@main
struct thecatapitestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
