//
//  WebService.swift
//  thecatapitest
//
//  Created by Pedro Ferreira on 18/09/21.
// MARK: WigiLabsTest
// Presente problemas para cargar la imagen de los gatos. La funcionalidad de votación no captura la fecha pero si el resultado(votación/Voted) y la raza.

import Foundation

class Webservice {
    
    func getPosts(completion: @escaping ([Breed]?) -> ()) {
        
        guard let url = URL(string: "https://api.thecatapi.com/v1/breeds") else {
            fatalError("Invalid URL")
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            guard let data = data, error == nil else {
                DispatchQueue.main.async {
                    completion(nil)
                }
                return
            }
            
            let posts = try? JSONDecoder().decode([Breed].self, from: data)
            
            DispatchQueue.main.async {
                completion(posts)
            }
            
        }.resume()
        
    }
    
}
